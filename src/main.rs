#![feature(plugin, custom_derive)]
#![plugin(rocket_codegen)]

extern crate hproto;
extern crate rocket;

use hproto::requests::fare_estimate as fe;
use hproto::requests::ride_request as rr;
use hproto::requests::near_by_drivers as nd;
use hproto::requests::driver_location as dl;

fn main() {
    rocket::ignite().mount("/", routes![
        fe::fare_estimate,
        rr::ride_request,
        nd::nearby_drivers,
        dl::driver_location
    ]).launch()
}
