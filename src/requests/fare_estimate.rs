use data::Location;
use rocket_contrib::JSON;

#[derive(Serialize, Deserialize, Debug)]
pub struct FareEstimateReq {
    pick_up_location: Location,
    drop_off_location: Location,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct FareEstimateRsp {
    fare_estimate: f64
}

#[post("/fare_estimate", format="application/json", data = "<req>")]
pub fn fare_estimate(req: JSON<FareEstimateReq>) -> JSON<FareEstimateRsp> {
    let fare_request = req.into_inner();
    println!("{:#?}", fare_request);

    let distance = fare_request.pick_up_location.distance(&fare_request.drop_off_location);

    (distance.round() * 0.5f64).to_string();

    JSON(FareEstimateRsp{fare_estimate: distance.round()})
}
