use data::Location;
use rocket_contrib::JSON;

#[derive(Serialize, Deserialize, Debug)]
pub struct NearByDriversReq {
    device_location: Location
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Driver {
    pub id: i32,
    pub name: String
}

#[derive(Serialize, Deserialize, Debug)]
pub struct NearByDriversRsp {
    drivers: Vec<Driver>
}

#[post("/nearby_drivers", format = "application/json", data="<req>")]
pub fn nearby_drivers(req: JSON<NearByDriversReq>) -> JSON<NearByDriversRsp> {
    let drivers_req = req.into_inner();
    println!("{:#?}", drivers_req);

    JSON(NearByDriversRsp{drivers: vec![Driver{id: 1, name: "Nihar".to_string()}, Driver{id: 2, name: "Nihar2".to_string()}]})
}
