use std::collections::HashMap;
use data::Location;
use rocket_contrib::JSON;
use rocket::State;

use std::sync::Mutex;

#[derive(Serialize, Deserialize, Debug)]
pub struct DriverLocationReq {
    id: i32,
    device_location: Location
}

#[derive(Serialize, Deserialize, Debug)]
pub struct DriverLocationRsp {
    id: i32,
    driver_location: Location
}

lazy_static! {
    static ref DB: Mutex<HashMap<i32, Location>> = {
        let mut m = HashMap::new();
        Mutex::new(m)
    };

    static ref DEVICE_LOCATION: Mutex<Location> = {
        let mut l = Location{lat: 0.0, lng: 0.0};
        Mutex::new(l)
    };
}

#[post("/driver_location", format = "application/json", data="<req>")]
pub fn driver_location(req: JSON<DriverLocationReq>) -> JSON<DriverLocationRsp> {
    let mut driver_location = Location{lat: 0.0, lng: 0.0};
    let mut map = DB.lock().unwrap();

    if req.device_location != *DEVICE_LOCATION.lock().unwrap() {
        *DEVICE_LOCATION.lock().unwrap() = req.device_location.clone();
        map.clear()
    }

    if !map.contains_key(&req.id) {
        driver_location = req.device_location.random_nearby_location()
    } else {
        let previous_location = map.get(&req.id).unwrap();
        driver_location = previous_location.move_by(10);
    }

    map.insert(req.id, driver_location.clone());

    JSON(DriverLocationRsp{id: req.id, driver_location: driver_location})
}
