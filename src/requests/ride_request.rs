use data::Location;
use requests::near_by_drivers::Driver;
use rocket_contrib::JSON;

#[derive(Serialize, Deserialize, Debug)]
pub struct RideRequestReq {
    pick_up_location: Location,
    drop_off_location: Location
}

#[derive(Serialize, Deserialize, Debug)]
pub struct RideDetails {
    driver: Driver,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct RideRequestRsp {
    ride_details: RideDetails,
}

#[post("/ride_request", format = "application/json", data="<req>")]
pub fn ride_request(req: JSON<RideRequestReq>) -> JSON<RideRequestRsp> {
    let ride_req = req.into_inner();
    println!("{:#?}", ride_req);

    JSON(RideRequestRsp{ride_details: RideDetails{driver: Driver{id: 1, name: "Nihar".to_string()}}})
}
