use rand;
use rand::{Rng};

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct Location {
    pub lat: f64,
    pub lng: f64
}

const RADIUS: f64 = 6371f64;

impl Location {
    // Gets the shortest distance between self and the given location in Kilometers
    pub fn distance(&self, location: &Location) -> f64 {
        let d_lat = (location.lat - self.lat).to_radians();
        let d_lng = (location.lng - self.lng).to_radians();

        let a1 = (d_lat/2f64).sin().powi(2);
        let a2 = (d_lng/2f64).sin().powi(2) * self.lat.to_radians().cos() * location.lat.to_radians().cos();

        let a = a1 + a2;

        let c = 2f64 * (a.sqrt()/(1f64 - a).sqrt()).atan();

        RADIUS * c
    }

    pub fn random_nearby_location(&self) -> Location {
        // Basically gets how much to change lat to get a max distance of 400-500m approx
        let lat_diff = rand::thread_rng().gen_range::<f64>(-0.002, 0.002);
        let lng_diff = rand::thread_rng().gen_range::<f64>(-0.002, 0.002);

        Location{lat: self.lat + lat_diff, lng: self.lng + lng_diff}
    }

    pub fn move_by(&self, distance: i32) -> Location {
        //Returns something that is about 'distance' meters away
        let diff = distance as f64 * 0.00001;

        let mut new_lat = self.lat + diff;
        if rand::thread_rng().gen() {
            new_lat = self.lat - diff
        }

        let mut new_lng = self.lng + diff;
        if rand::thread_rng().gen() {
            new_lng = self.lng - diff
        }

        Location{lat: new_lat, lng: new_lng}
    }
}
