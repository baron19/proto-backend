#![feature(plugin, custom_derive)]
#![plugin(rocket_codegen)]

extern crate rocket;
extern crate rocket_contrib;
extern crate rand;

#[macro_use] extern crate lazy_static;

extern crate serde_json;
#[macro_use] extern crate serde_derive;

pub mod data;
pub mod requests;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
    }
}
